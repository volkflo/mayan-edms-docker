Changelog
=========

2017-02-25
----------
- Update to Mayan EDMS 2.1.10.
- Add upgrade instructions.
- Add customization instructions.

2017-01-06
----------
- Add Vagrantfile to build and test Docker image.

2016-11-23
----------
- Update to Mayan EDMS version 2.1.6.
- Add HISTORY.md file.

