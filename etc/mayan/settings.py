import os

ALLOWED_HOSTS = ['*']

BROKER_URL = 'redis://127.0.0.1:6379/0'
CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379/0'

if os.environ.get('MAYAN_USE_POSTGRES', False):
    from .postgres import DATABASES
